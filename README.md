# Inheritance Algorithm

_Inheritance algorithm_ is a command line program written in [Python](https://www.python.org/) that allows to attribute to each clonal group (CG), an identifier that would maximally reflect the widely adopted 7-gene ST identifier of the corresponding isolates.

Development a set of naming rules that prioritize the most abundant ST observed among isolates of each CG, as well as some supplementary rules in case of ties. This algorithm is summarized below, whereas an example is given in the technical notes pdf file.


## Installation and execution

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/BEBP/inheritance-algorithm.git
```

Verify that [Python](https://www.python.org/downloads/) (3.6 or higher) is installed, as well as [Pandas](https://pandas.pydata.org/) (x or higher) and [NetworkX](https://networkx.org/)

Execute the file `InheritanceAlgorithm.py` available inside the _src_ directory with the following command line model:

```bash
python InheritanceAlgorithm.py [options]
```

## Usage

Launch _InheritanceAlgorithm_ with option `-h` to read the following documentation:

```
usage: InheritanceAlgorithm [-h] -i FILEINPUT -c COLUMN_CG -o FILEOUTPUT 

This tool to attribute to each clonal group (CG), an identifier that would maximally reflect the widely adopted 7-gene ST identifier of the corresponding isolates.

optional arguments:
  -h, --help       show this help message and exit
  -i FILEINPUT     (mandatory) input tab-delimited file containing the CGs associated for each isolate
  -c COLUMN_CG     (mandatory) column(s) of the selected CG(s)
  -o FILEOUTPUT    (mandatory) basename for output files
  
```

## Example

The input file clustering.strain.txt inside the directory _example_ contains three columns; the first one corresponds to the strain identifiers, the next two correspond to the associated CGs. 

The input file clustering.strain.txt in the _example_ directory contains five columns (id, isolate, ST, sublineage, clonalgroup). The id and isolate columns are the identifiers of the strains. The ST column is the 7-gene ST identifier of the corresponding isolates. The sublineage and clonalgroup columns are those to be mapped according to the ST nomenclature. 
For this example, three files are created. The first _result.out_ is similar to the input tabulated file with the mapping of all the groups with their allocation characteristics. Two other files are created (_result.sublineage.txt_ and _result.clonalgroup.txt_), either for one for each group, a tab-delimited file with a column for the old name of the group, a second one for the name mapped to the ST, and finally its attribution.  

```bash
python InheritanceAlgorithm.py -i clustering.strain.txt -c sublineage,clonalgroup -o result 
```

## References

A dual barcoding approach to bacterial strain nomenclature: Genomic taxonomy of Klebsiella pneumoniae strains
Melanie Hennart, Julien Guglielmini, Martin C.J. Maiden, Keith A. Jolley, Alexis Criscuolo, Sylvain Brisse
bioRxiv 2021.07.26.453808; doi: https://doi.org/10.1101/2021.07.26.453808

